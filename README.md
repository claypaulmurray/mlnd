# MLND
This is the repository for the Machine Learning Engineer NanoDegree program offered by Udacity. It has been cloned on my page to simplify submission and grading as well as illustrate some of my technical abilities in the field of programming, machine learning, and data science.

It is currently underway, but several projects have already been completed, such as finding_donors, customer_segments, and boston_housing.
